FROM openjdk:8-jre-alpine

COPY target/HelloWorld-*.jar /app.jar
ENTRYPOINT java -Dserver.tomcat.protocol-header=x-forwarded-proto -Dserver.tomcat.remote-ip-header=x-forwarded-for -jar /app.jar
